package com.company;

public abstract class AbstractZone {

    protected String nameZone;

    protected int numLocalities;

    protected int freeLocalities;

    protected float normalPrice;

    protected float reducedPrice;

    protected float improvedPrice;

    protected float collection;

    protected float lastPrice;

    AbstractZone(String nameZone, int numLocalities, float normalPrice, float reducedPrice, float improvedPrice) {

        this.nameZone = nameZone;
        this.numLocalities = numLocalities;
        this.freeLocalities = numLocalities;
        this.normalPrice = normalPrice;
        this.reducedPrice = reducedPrice;
        this.improvedPrice = improvedPrice;
    }

    public boolean isFull() {

        return this.freeLocalities == 0;
    }

    public void substractLocality() {

        this.freeLocalities -= 1;
    }

    public int soldTickets() {

        return this.numLocalities - this.freeLocalities;
    }

    public void addNormalCollection() {

        this.lastPrice = this.normalPrice;

        this.collection += this.normalPrice;
    }

    public void addReducedCollection() {

        this.lastPrice = this.reducedPrice;

        this.collection += this.reducedPrice;
    }

    public void addImprovedCollection() {

        this.lastPrice = this.improvedPrice;

        this.collection += this.improvedPrice;
    }

    public float getNormalPrice() {

        return this.normalPrice;
    }

    public float getReducedPrice() {

        return this.reducedPrice;
    }

    public float getImprovedPrice() {

        return this.improvedPrice;
    }

    public float getCollection() {

        return this.collection;
    }

    public float getLastPrice() {

        return this.lastPrice;
    }

    public String getNameZone() {

        return this.nameZone;
    }
}
