package com.company;

public class Ticket {

    private int identifier;

    private AbstractZone zone;

    private String buyerName;

    Ticket(int identifier, AbstractZone zone, String buyerName) {

        this.identifier = identifier;
        this.zone = zone;
        this.buyerName = buyerName;
    }

    public int getIdentifier() {

        return this.identifier;
    }

    public AbstractZone getZone() {

        return this.zone;
    }

    public String getBuyerName() {

        return this.buyerName;
    }
}
